//
// Created by tambu on 9/5/20.
//

#ifndef SOLUCION_RECORDATORIOS_H
#define SOLUCION_RECORDATORIOS_H

#include <list>
#include <ostream>
using namespace std;

class Fecha {
public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();

    bool operator==(const Fecha o) const ;
    bool operator<(const Fecha o) const ;
    bool operator>(const Fecha o) const ;

private:
    int mes_;
    int dia_;
};

class Horario {
public:
    Horario(uint hora, uint minuto);
    uint hora();
    uint min();

    bool operator<(const Horario h) const ;
    bool operator>(const Horario h) const ;

private:
    uint hora_;
    uint min_;
};

class Recordatorio {
public:
    Recordatorio(Fecha f, Horario h, string evento);
    Fecha fecha();
    Horario horario();
    string evento();

    bool operator>(const Recordatorio r) const ;
    bool operator<(const Recordatorio r) const ;

private:
    Fecha fecha_;
    Horario horario_;
    string evento_;
};


class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();


private:
    Fecha fechaActual_;
    map<Fecha, list<Recordatorio>> recor_;
};

#endif //SOLUCION_RECORDATORIOS_H
